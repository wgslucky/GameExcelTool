${packagePath}

<#list importPackageList as pck>
${pck}
</#list>
/**
 * @Author 王广帅  QQ群：66728073
 */
public class ${className} {
<#list classFields as fd>
   /**
   * ${fd.comment}
   */
   private ${fd.type} ${fd.name};
</#list>

<#list classFields as fd>

   /**
   * ${fd.comment}
   */
   <#if fd.type == 'boolean'>
   public ${fd.type} is${fd.getterName}(){
        return ${fd.name};
   }
   <#else>
   public ${fd.type} get${fd.getterName}(){
        return ${fd.name};
   }
   </#if>
   public void set${fd.getterName}(${fd.type} ${fd.name}){
        this.${fd.name} = ${fd.name};
   }
</#list>


}
