package com.xinyue.exceltool;

import com.xinyue.exceltool.logic.ReadExcelService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class GameExcelToolMain {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(GameExcelToolMain.class);
        app.setWebApplicationType(WebApplicationType.NONE);
        ApplicationContext context = app.run(args);

        ReadExcelService readExcelService = context.getBean(ReadExcelService.class);
        readExcelService.printConfig();
        readExcelService.checkExcelFilePath();
        readExcelService.readExcel();
    }


}
