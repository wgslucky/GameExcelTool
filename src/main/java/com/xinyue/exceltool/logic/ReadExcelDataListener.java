package com.xinyue.exceltool.logic;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.xinyue.exceltool.config.ExcelToolConfig;
import com.xinyue.exceltool.logic.codeclass.ICodeClassFactory;
import com.xinyue.exceltool.logic.codeclass.javacode.GenerateJavaClassFactory;
import com.xinyue.exceltool.logic.model.ExcelClassData;
import com.xinyue.exceltool.logic.model.TitleCellData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * 读取Excel表头的监听实现类。以Excel表的前N行做为表头信息，读取固定的配置信息，包括，字段名，字段类型，注释等。
 *
 * @author 王广帅
 */
public class ReadExcelDataListener extends AnalysisEventListener<Map<Integer, String>> {

    private Logger logger = LoggerFactory.getLogger(ReadExcelDataListener.class);
    private ExcelClassData excelClassData;
    private String excelFileName;
    private ExcelToolConfig excelToolConfig;
    private ExcelToServerJsonManager excelToServerJsonManager;

    public ReadExcelDataListener(String excelFileName, ExcelToolConfig excelToolConfig) {
        this.excelFileName = excelFileName;
        this.excelClassData = new ExcelClassData();
        this.excelClassData.setFileName(this.excelFileName);
        this.excelToolConfig = excelToolConfig;
        this.excelToServerJsonManager = new ExcelToServerJsonManager(excelClassData, excelToolConfig);
    }

    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
        if (excelToolConfig.isServerEnable()) {
            //根据读取到的内容，生成服务器端使用的json文件。
            this.excelToServerJsonManager.appendRowDataToJsonFile(data);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        excelToServerJsonManager.forceFlush();
        ICodeClassFactory codeClassFactory;
        if (excelToolConfig.isServerEnable()) {
            codeClassFactory = new GenerateJavaClassFactory(this.excelClassData, this.excelToolConfig);
            codeClassFactory.createClass();
        } else {
            //生成其它语言的配置类
        }

    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        //当前读取的行数，以0为第一索引
        int row = context.readRowHolder().getRowIndex();
        //读取每一列的数据
        headMap.forEach((col, value) -> {
            if (!StringUtils.isEmpty(value) && !("$").equals(value)) {
                TitleCellData titleCellData = excelClassData.getTitleCellDataMap().computeIfAbsent(col,
                        key -> new TitleCellData());
                value = value.trim();
                // 第一行，类字段名
                if (row == 0) {
                    value = StringUtils.uncapitalize(value);
                    titleCellData.setFieldName(value);
                }
                // 第二行，类字段类型
                if (row == 1) {
                    checkFieldType(value, col);
                    titleCellData.setFieldType(value.toLowerCase());
                }
                // 第三行，类字段注释说明
                if (row == 2 && titleCellData.getFieldName() != null) {
                    titleCellData.setComment(value);
                    titleCellData.setScope("cs");
                }
            }
        });
    }
    
    private void checkFieldType(String fieldType, int col) {
        if (StringUtils.isEmpty(fieldType)) {
            throw new IllegalArgumentException("字段类型不能为空,表名: " + this.excelFileName + ",列数:" + col);
        }
    }
}
