package com.xinyue.exceltool.logic.model;

import java.util.Map;
import java.util.TreeMap;

/**
 * 表数据的类信息
 *
 * @author 王广帅
 */
public class ExcelClassData {

    private String fileName;
    /**
     * key是列数，从0开始
     */
    private Map<Integer, TitleCellData> titleCellDataMap = new TreeMap<>();

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Map<Integer, TitleCellData> getTitleCellDataMap() {
        return titleCellDataMap;
    }

    public void setTitleCellDataMap(Map<Integer, TitleCellData> titleCellDataMap) {
        this.titleCellDataMap = titleCellDataMap;
    }

}
