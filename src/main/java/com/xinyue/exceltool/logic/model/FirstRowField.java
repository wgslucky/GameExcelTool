package com.xinyue.exceltool.logic.model;

/**
 * @author 王广帅
 * @since 2021/4/30 5:50 下午
 */
public class FirstRowField {
    /**
     * excel中配置的字段名
     */
    private String fieldName;
    /**
     * 字段名的注释
     */
    private String comment;

    public FirstRowField(String fieldName, String comment) {
        this.fieldName = fieldName;
        this.comment = comment;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getComment() {
        return comment;
    }
}
