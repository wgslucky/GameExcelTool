package com.xinyue.exceltool.logic.model;

public class TitleCellData {
    private String fieldName; // 生成类时的字段名
    private String fieldType; // 生成类时的字段类型
    private String scope; // 此字段的有效范围，c 表示客户端，s 表示服务端,cs表示客户端与服务端
    // 注释
    private String comment;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isServerType() {
        return "cs".equals(this.scope) || "s".equals(this.scope);
    }

    public boolean isClientFileType() {
        return "cs".equals(this.scope) || "c".equals(this.scope);
    }

}
