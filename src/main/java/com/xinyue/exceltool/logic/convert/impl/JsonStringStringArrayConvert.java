package com.xinyue.exceltool.logic.convert.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import com.xinyue.exceltool.logic.convert.ValueConvertUtil;


/**
 * @author 王广帅
 * @since 2021/7/29 10:42 下午
 **/
public class JsonStringStringArrayConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {

        String tempValue = ValueConvertUtil.replaceChineseComma(value);
        String[] array_item = tempValue.split(";");
        JSONArray array = new JSONArray();
        for (String item : array_item) {
            String[] strData = ValueConvertUtil.splitByComma(tempValue);
            String key = strData[0];
            String strValue = strData[1];
            JSONObject obj = new JSONObject();
            obj.put(key, strValue);
            array.add(obj);
        }
        return array;
    }

    @Override
    public String getImpartPackage() {
        return "import com.alibaba.fastjson.JSONArray;";
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "JSONArray";
    }

    @Override
    public boolean isDbJson() {
        return true;
    }
}
