package com.xinyue.exceltool.logic.convert.impl;

import com.alibaba.fastjson.JSONArray;
import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import com.xinyue.exceltool.logic.convert.ValueConvertUtil;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 王广帅
 * @since 2021/5/9 0:09
 */
public class FloatArrayConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        if (!value.startsWith("[")) {
            String tempValue = ValueConvertUtil.replaceChineseComma(value);
            String[] strValue = ValueConvertUtil.splitByComma(tempValue);
            List<Float> array = Arrays.stream(strValue).filter(c -> !StringUtils.isEmpty(c)).map(c -> Float.parseFloat(c)).collect(Collectors.toList());
            return array;
        } else {
            JSONArray jsonArray = JSONArray.parseArray(value);
            return jsonArray.toJavaList(Float.class);
        }
    }

    @Override
    public String getImpartPackage() {
        return "import java.util.List;";
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "List<Float>";
    }
}
