package com.xinyue.exceltool.logic.convert;

/**
 * @author 王广帅
 * @since 2021/5/9 0:12
 */
public class ValueConvertUtil {
    /**
     * 将字符串中的中文逗号转成英文逗号
     *
     * @param value
     * @return
     */
    public static String replaceChineseComma(String value) {
        return value.replace("，", ",");
    }

    public static String[] splitByComma(String value) {
        return value.split(",");
    }


    public static boolean isTrue(String value) {
        return "true".equalsIgnoreCase(value);
    }
}
