package com.xinyue.exceltool.logic.convert.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import org.springframework.util.StringUtils;

/**
 * @author 王广帅
 * @since 2021/5/8 23:52
 */
public class JsonObjConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        JSONObject data = JSON.parseObject(value);
        return data;
    }

    @Override
    public String getImpartPackage() {
        return "import com.alibaba.fastjson.JSONObject;";
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "JSONObject";
    }

    @Override
    public boolean isDbJson() {
        return true;
    }
}
