package com.xinyue.exceltool.logic.convert.impl;

import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import org.springframework.util.StringUtils;

/**
 * @author 王广帅
 * @since 2021/5/8 23:39
 */
public class LongConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return 0L;
        }
        long longValue = Long.parseLong(value);
        return longValue;
    }
}
