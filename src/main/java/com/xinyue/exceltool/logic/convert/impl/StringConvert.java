package com.xinyue.exceltool.logic.convert.impl;


import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;

/**
 * <p>
 *
 * </p>
 *
 * @author 王广帅
 * @since 2021/5/6 4:10 下午
 */
public class StringConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        return value;
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "String";
    }
}
