package com.xinyue.exceltool.logic.convert.impl;
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import com.xinyue.exceltool.logic.convert.ValueConvertUtil;
import org.springframework.util.StringUtils;

/**
 * @author 王广帅
 * @since 2021/5/9 0:19
 */
public class BooleanConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        return ValueConvertUtil.isTrue(value);
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "boolean";
    }
}
