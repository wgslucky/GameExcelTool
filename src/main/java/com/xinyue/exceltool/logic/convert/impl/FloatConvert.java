package com.xinyue.exceltool.logic.convert.impl;

import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import org.springframework.util.StringUtils;

/**
 * @author 王广帅
 * @since 2021/5/9 0:07
 */
public class FloatConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return 0F;
        }
        return Float.parseFloat(value);
    }
}
