package com.xinyue.exceltool.logic.convert.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import org.springframework.util.StringUtils;

/**
 * @author 王广帅
 * @since 2021/5/8 23:54
 */
public class JsonArrayConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        JSONArray array = JSON.parseArray(value);
        return array;
    }

    @Override
    public String getImpartPackage() {
        return "import com.alibaba.fastjson.JSONArray;";
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "JSONArray";
    }

}
