/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.xinyue.exceltool.logic.convert;


import com.xinyue.exceltool.logic.convert.impl.*;

import java.util.function.Supplier;

/**
 * <p>
 * 数据类型配置枚举类，并添加了数据类型转换接口
 * </p>
 *
 * @author 王广帅
 * @since 2021/5/1 2:34 上午
 */
public enum CellValueTypeStrategyEnum {
    /**
     * int类型
     */
    INT("int", () -> new IntConvert()),
    /**
     * int 数组类型
     */
    INT_ARRAY("[int]", () -> new IntArrayConvert()),
    /**
     * long 类型
     */
    LONG("long", () -> new LongConvert()),
    /**
     * long 数组
     */
    LONG_ARRAY("[long]", () -> new LongArrayConvert()),
    /**
     * string 类型
     */
    STRING("string", () -> new StringConvert()),
    /**
     * string 数组
     */
    STRING_ARRAY("[string]", () -> new StringArrayConvert()),
    /**
     * json object 类型
     */
    JSON_OBJ("json_obj", () -> new JsonObjConvert()),
    /**
     * json 数组
     */
    JSON_ARRAY("json_array", () -> new JsonArrayConvert()),
    /**
     * float 类型
     */
    FLOAT("float", () -> new FloatConvert()),
    /**
     * float 数组
     */
    FLOAT_ARRAY("[float]", () -> new FloatArrayConvert()),
    /**
     * boolean 类型
     */
    BOOLEAN("bool", () -> new BooleanConvert()),
    /**
     * byte类型
     */
    BYTE("byte", () -> new ByteConvert()),
    /**
     * json数组，数组key-value， key为字符串，value为int
     */
    JSON_STRING_INT_ARRAY("[string,int]", () -> new JsonStringIntArrayConvert()),
    /**
     * json数组，数组key-value， key为字符串，value为字符串
     */
    JSON_STRING_STRING_ARRAY("[string,string]", () -> new JsonStringStringArrayConvert());
    private String type;
    private Supplier<ICellValueTypeStrategy> cellValueConvert;

    CellValueTypeStrategyEnum(String type, Supplier<ICellValueTypeStrategy> cellValueConvert) {
        this.type = type;
        this.cellValueConvert = cellValueConvert;
    }

    public String getType() {
        return type;
    }

    public Supplier<ICellValueTypeStrategy> getCellValueConvert() {
        return cellValueConvert;
    }

    /**
     * 获取配置值的转换
     *
     * @param type 数值类型
     * @return 配置数值转换策略
     */
    public static ICellValueTypeStrategy getValueConvert(String type) {
        for (CellValueTypeStrategyEnum convertTypeEnum : CellValueTypeStrategyEnum.values()) {
            if (convertTypeEnum.getType().equals(type)) {
                return convertTypeEnum.getCellValueConvert().get();
            }
        }
        throw new IllegalArgumentException("不存在的数据类型:" + type);
    }
}
