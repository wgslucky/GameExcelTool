package com.xinyue.exceltool.logic.convert.impl;


import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;

public class ByteConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        return Byte.parseByte(value);
    }
}
