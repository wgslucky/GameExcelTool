package com.xinyue.exceltool.logic.convert.impl;

import com.alibaba.fastjson.JSONArray;
import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import com.xinyue.exceltool.logic.convert.ValueConvertUtil;
import org.springframework.util.StringUtils;

/**
 * @author 王广帅
 * @since 2021/5/8 23:50
 */
public class StringArrayConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        if (value.startsWith("[")) {
            JSONArray array = JSONArray.parseArray(value);
            return array.toJavaList(String.class);
        } else {
            String tempValue = ValueConvertUtil.replaceChineseComma(value);
            String[] array = ValueConvertUtil.splitByComma(tempValue);
            return array;
        }
    }

    @Override
    public String getImpartPackage() {
        return "import java.util.List;";
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "List<String>";
    }
}
