package com.xinyue.exceltool.logic.convert.impl;

import com.alibaba.fastjson.JSONArray;
import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import com.xinyue.exceltool.logic.convert.ValueConvertUtil;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 王广帅
 * @since 2021/5/8 23:42
 */
public class LongArrayConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        if (value.startsWith("[")) {
            JSONArray array = JSONArray.parseArray(value);
            return array.toJavaList(Long.class);
        }
        //兼容一下中文逗号，虽然强调让策划和数值配置的时候要用英文逗号，但是他们也有可能不按约定来
        String tempValue = ValueConvertUtil.replaceChineseComma(value);
        String[] strData = ValueConvertUtil.splitByComma(tempValue);
        //对于json串来说，Long[]和List<Long> 没什么区别
        List<Long> longData = Arrays.stream(strData).filter(s -> s != null).map(s -> Long.parseLong(s)).collect(Collectors.toList());

        return Collections.unmodifiableList(longData);
    }

    @Override
    public String getImpartPackage() {
        return "import java.util.List;";
    }

    @Override
    public String getJavaFieldTypeName(String type) {
        return "List<Long>";
    }
}
