package com.xinyue.exceltool.logic.convert;

/**
 * <p>
 * 将Excel每个格子的数据，根据类型转化为对应的值，这里是一个转化的统一接口
 * </p>
 *
 * @author 王广帅
 * @since 2021/5/1 2:34 上午
 */
public interface ICellValueTypeStrategy {
    /**
     * 将Excel中读取的值转化为对应的类型的数值
     *
     * @param value 要转换的值
     * @return 转换之后的对象
     */
    Object convert(String value);

    /**
     * 获取每个类型的包引用路径
     *
     * @return 引入的包路径
     */
    default String getImpartPackage() {
        return null;
    }

    /**
     * 获取对应的java字段类型的名字
     *
     * @param type 配置的数据类型
     * @return java类型的名字
     */
    default String getJavaFieldTypeName(String type) {
        return type;
    }

    default boolean isDbJson() {
        return false;
    }
}
