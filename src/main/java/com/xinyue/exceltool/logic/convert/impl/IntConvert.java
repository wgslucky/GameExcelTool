package com.xinyue.exceltool.logic.convert.impl;

import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import org.springframework.util.StringUtils;

/**
 * <p>
 *
 * </p>
 *
 * @author 王广帅
 * @since 2021/5/1 2:39 上午
 */
public class IntConvert implements ICellValueTypeStrategy {
    @Override
    public Object convert(String value) {
        if (StringUtils.isEmpty(value)) {
            return 0;
        }
        return Integer.parseInt(value);
    }
}
