package com.xinyue.exceltool.logic;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.xinyue.exceltool.config.ExcelToolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * 读取一个Excel文件的管理类
 *
 * @author wgs
 */
public class ReadOneExcelManager {

    private static Logger logger = LoggerFactory.getLogger(ReadOneExcelManager.class);
    private File excelFile;
    private static final int HEAD_ROW_COUNT = 5;
    private ExcelToolConfig excelToolConfig;

    public ReadOneExcelManager(File excelFile, ExcelToolConfig excelToolConfig) {
        this.excelFile = excelFile;
        this.excelToolConfig = excelToolConfig;
    }

    /**
     * 读取excel配置头部信息
     */
    public void readExcelData() {
        ExcelReader excelReader = EasyExcel.read(excelFile).build();
        ReadExcelDataListener readExcelDataListener = new ReadExcelDataListener(this.excelFile.getName(), excelToolConfig);
        // 从第一个表中读取信息
        ReadSheet readSheet = EasyExcel.readSheet(0).headRowNumber(HEAD_ROW_COUNT).registerReadListener(readExcelDataListener).build();
        excelReader.read(readSheet);
        excelReader.finish();
    }

}
