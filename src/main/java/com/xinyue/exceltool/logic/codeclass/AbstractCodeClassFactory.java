/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.xinyue.exceltool.logic.codeclass;


import com.xinyue.exceltool.config.ExcelToolConfig;
import com.xinyue.exceltool.logic.model.ExcelClassData;

/**
 * @author 王广帅
 * @since 2021/5/9 11:10
 */
public abstract class AbstractCodeClassFactory implements ICodeClassFactory {
    private ExcelClassData excelClassData;
    private ExcelToolConfig excelToolConfig;

    public AbstractCodeClassFactory(ExcelClassData excelClassData, ExcelToolConfig excelToolConfig) {
        this.excelClassData = excelClassData;
        this.excelToolConfig = excelToolConfig;
    }

    public ExcelClassData getExcelClassData() {
        return excelClassData;
    }

    public ExcelToolConfig getExcelToolConfig() {
        return excelToolConfig;
    }
}
