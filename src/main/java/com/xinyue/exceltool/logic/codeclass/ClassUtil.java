/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.xinyue.exceltool.logic.codeclass;

import org.springframework.util.StringUtils;

public class ClassUtil {
    /**
     * 从文件名中获取类名
     *
     * @param fileName excel配置文件名
     * @return 对应的类名
     */
    public static String getClassName(String fileName) {
        String[] splites = fileName.split("\\.");
        String className = StringUtils.capitalize(splites[0]);
        return className + "Config";
    }
}
