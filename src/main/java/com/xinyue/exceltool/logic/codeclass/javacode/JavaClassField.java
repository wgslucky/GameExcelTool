package com.xinyue.exceltool.logic.codeclass.javacode;

/**
 * @Author 王广帅
 * @Date 2021/5/9 11:22
 */
public class JavaClassField {
    private String type;//字段类型
    private String name;//字段名
    private String getterName;
    private String comment;//注释
    /**
     * 在数据库是否为json格式
     */
    private boolean dbJson;


    public JavaClassField(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGetterName() {
        return getterName;
    }

    public void setGetterName(String getterName) {
        this.getterName = getterName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isDbJson() {
        return dbJson;
    }

    public void setDbJson(boolean dbJson) {
        this.dbJson = dbJson;
    }
}
