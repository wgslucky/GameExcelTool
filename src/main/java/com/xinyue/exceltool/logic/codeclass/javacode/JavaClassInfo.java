package com.xinyue.exceltool.logic.codeclass.javacode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author 王广帅
 * @Date 2021/5/9 11:20
 */
public class JavaClassInfo {
    //类名
    private String className;
    private String tableName;
    private String packagePath;
    //存储java类中，包引用的信息
    private Set<String> importPackageList = new HashSet<>();
    //存储java类中字段的类型与名字
    private List<JavaClassField> classFields = new ArrayList<>();

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public Set<String> getImportPackageList() {
        return importPackageList;
    }

    public List<JavaClassField> getClassFields() {
        return classFields;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
