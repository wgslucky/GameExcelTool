package com.xinyue.exceltool.logic.codeclass.javacode;


import cn.hutool.core.util.StrUtil;
import com.xinyue.exceltool.config.ExcelToolConfig;
import com.xinyue.exceltool.logic.CodeTemplateService;
import com.xinyue.exceltool.logic.codeclass.AbstractCodeClassFactory;
import com.xinyue.exceltool.logic.codeclass.ClassUtil;
import com.xinyue.exceltool.logic.convert.CellValueTypeStrategyEnum;
import com.xinyue.exceltool.logic.convert.ICellValueTypeStrategy;
import com.xinyue.exceltool.logic.model.ExcelClassData;
import org.springframework.util.StringUtils;

import java.io.File;

/**
 * 根据Excel配置，生成Java类代码
 *
 * @author 王广帅
 * @since 2021/5/9 11:04
 */

public class GenerateJavaClassFactory extends AbstractCodeClassFactory {
    private JavaClassInfo javaClassInfo = new JavaClassInfo();

    public GenerateJavaClassFactory(ExcelClassData excelClassData, ExcelToolConfig excelToolConfig) {
        super(excelClassData, excelToolConfig);
    }


    @Override
    public void createClass() {
        String className = ClassUtil.getClassName(getExcelClassData().getFileName());
        this.javaClassInfo.setClassName(className);
        //添加最上面的package路径
        this.javaClassInfo.setPackagePath("package " + getExcelToolConfig().getJavaClassPackage() + ";");
        addImportPackage();
        addJavaFiled();
        writeFile();
    }

    /**
     * 添加Java类的包引用
     */
    private void addImportPackage() {
        getExcelClassData().getTitleCellDataMap().values().forEach(titleCellData -> {
            if (titleCellData.getFieldName() != null && titleCellData.getFieldType() != null) {
                ICellValueTypeStrategy valueConvert = CellValueTypeStrategyEnum.getValueConvert(titleCellData.getFieldType());
                String importPackage = valueConvert.getImpartPackage();
                if (importPackage != null) {
                    if (!this.javaClassInfo.getImportPackageList().contains(importPackage)) {
                        this.javaClassInfo.getImportPackageList().add(importPackage);
                    }
                }
            }
        });
    }

    private void addJavaFiled() {
        getExcelClassData().getTitleCellDataMap().values().forEach(titleCellData -> {
            if (titleCellData.getFieldName() != null && titleCellData.getFieldType() != null) {
                ICellValueTypeStrategy valueConvert = CellValueTypeStrategyEnum.getValueConvert(titleCellData.getFieldType());
                String fieldTypeName = valueConvert.getJavaFieldTypeName(titleCellData.getFieldType());
                JavaClassField javaClassField = new JavaClassField(fieldTypeName, titleCellData.getFieldName());
                String getterName = StringUtils.capitalize(javaClassField.getName());
                javaClassField.setGetterName(getterName);
                javaClassField.setComment(titleCellData.getComment());
                javaClassField.setDbJson(valueConvert.isDbJson());
                this.javaClassInfo.getClassFields().add(javaClassField);
            }
        });
    }

    private void writeFile() {
        String path = getExcelToolConfig().getServerClassPath();
        String className = this.javaClassInfo.getClassName().replace("_", "");
        String fileName = className + ".java";
        this.javaClassInfo.setClassName(className);
        String tableName = "t_" + StrUtil.toUnderlineCase(className);
        this.javaClassInfo.setTableName(tableName);
        File targetFile = new File(path, fileName);
        CodeTemplateService.outFile("java_template.ftl", targetFile, this.javaClassInfo);
    }
}
