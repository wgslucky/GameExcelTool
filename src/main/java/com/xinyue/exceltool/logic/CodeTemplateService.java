package com.xinyue.exceltool.logic;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * 模板信息配置
 *
 * @author wang guang shuai
 */
@Service
public class CodeTemplateService {

    private final static Logger LOGGER = LoggerFactory.getLogger(CodeTemplateService.class);
    private static Configuration config;

    /**
     * 初始化信息
     */
    @PostConstruct
    public void start() {
        config = new Configuration(Configuration.VERSION_2_3_30);
        try {
            config.setClassForTemplateLoading(this.getClass(), "/templates");
            config.setDefaultEncoding("UTF-8");
        } catch (Exception e) {
            LOGGER.error("初始化模板信息失败", e);
            System.exit(1);
        }
    }

    /**
     * 文件输出
     *
     * @param templateName 模板名字
     * @param targetFile   目标文件
     * @param data         要写到文件中的数据
     */
    public static void outFile(String templateName, File targetFile, Object data) {
        if (config == null) {
            LOGGER.error("未初始化配置信息");
            return;
        }
        try {

            Template template = config.getTemplate(templateName, "utf8");
            try (Writer writer = new FileWriter(targetFile)) {
                template.process(data, writer);
                writer.flush();
            } catch (Exception e) {
                LOGGER.error("根据模板生成文件失败", e);
                System.exit(0);
            }

        } catch (IOException e) {
            LOGGER.error("根据模板生成文件失败", e);
        }

    }
}
