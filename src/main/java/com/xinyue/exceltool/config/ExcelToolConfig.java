package com.xinyue.exceltool.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "excel.config")
public class ExcelToolConfig {
    /**
     * excel 文件所在的目录路径，默认是：config/excel
     */
    private String excelFilePath = "config/excel";
    /**
     * 生成的服务端的配置类目录路径，默认是：config/server_class
     */
    private String serverClassPath = "config/server_class";
    /**
     * 生成的客户端的配置类目录路径，默认是：config/client_class
     */
    private String clientClassPath = "config/client_class";
    /**
     * 生成的服务端的配置数据目录路径，默认是：config/server_data
     */
    private String serverDataPath = "config/server_data";
    /**
     * 生成的客户端配置数据目录路径，默认是：config/client_data;
     */
    private String clientDataPath = "config/client_data";
    /**
     * 是否生成服务端代码和数据
     */
    private boolean serverEnable = true;
    /**
     * 生成的Java代码的包路径
     */
    private String javaClassPackage = "com.xinyue.dataconfig";
    /**
     * 是否生成客户端代码和数据
     */
    private boolean clientEnable = true;
    /**
     * 读取Excel数据时，达到多少条时会刷新到json文件之中
     */
    private int dataFlushThreshold = 3000;

    private List<String> serverTables = new ArrayList<>();

    public List<String> getServerTables() {
        return serverTables;
    }

    public void setServerTables(List<String> serverTables) {
        this.serverTables = serverTables;
    }

    public boolean isServerEnable() {
        return serverEnable;
    }

    public void setServerEnable(boolean serverEnable) {
        this.serverEnable = serverEnable;
    }

    public boolean isClientEnable() {
        return clientEnable;
    }

    public void setClientEnable(boolean clientEnable) {
        this.clientEnable = clientEnable;
    }

    public String getClientDataPath() {
        return clientDataPath;
    }

    public void setClientDataPath(String clientDataPath) {
        this.clientDataPath = clientDataPath;
    }

    public String getExcelFilePath() {
        return excelFilePath;
    }

    public void setExcelFilePath(String excelFilePath) {
        this.excelFilePath = excelFilePath;
    }

    public String getServerClassPath() {
        return serverClassPath;
    }

    public void setServerClassPath(String serverClassPath) {
        this.serverClassPath = serverClassPath;
    }

    public String getClientClassPath() {
        return clientClassPath;
    }

    public void setClientClassPath(String clientClassPath) {
        this.clientClassPath = clientClassPath;
    }

    public String getServerDataPath() {
        return serverDataPath;
    }

    public void setServerDataPath(String serverDataPath) {
        this.serverDataPath = serverDataPath;
    }

    public int getDataFlushThreshold() {
        return dataFlushThreshold;
    }

    public void setDataFlushThreshold(int dataFlushThreshold) {
        this.dataFlushThreshold = dataFlushThreshold;
    }

    public String getJavaClassPackage() {
        return javaClassPackage;
    }

    public void setJavaClassPackage(String javaClassPackage) {
        this.javaClassPackage = javaClassPackage;
    }

    @Override
    public String toString() {
        return "ExcelToolConfig{" +
                "excelFilePath='" + excelFilePath + '\'' +
                ", serverClassPath='" + serverClassPath + '\'' +
                ", clientClassPath='" + clientClassPath + '\'' +
                ", serverDataPath='" + serverDataPath + '\'' +
                ", clientDataPath='" + clientDataPath + '\'' +
                ", serverEnable=" + serverEnable +
                ", javaClassPackage='" + javaClassPackage + '\'' +
                ", clientEnable=" + clientEnable +
                ", dataFlushThreshold=" + dataFlushThreshold +
                '}';
    }
}
