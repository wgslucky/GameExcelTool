package com.xinyue.dataconfig;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * @Author 王广帅  QQ群：66728073
 */
public class ItemConfig {
    private String id;
    private String name;
    private int days;
    private int[] test1;
    private String[] test2;
    private JSONObject test3;
    private JSONArray test4;
    private boolean test5;


    /**
     *
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * 名字
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     */
    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    /**
     * 测试字段1
     */
    public int[] getTest1() {
        return test1;
    }

    public void setTest1(int[] test1) {
        this.test1 = test1;
    }

    /**
     * 测试字段2
     */
    public String[] getTest2() {
        return test2;
    }

    public void setTest2(String[] test2) {
        this.test2 = test2;
    }

    /**
     * 测试字段3
     */
    public JSONObject getTest3() {
        return test3;
    }

    public void setTest3(JSONObject test3) {
        this.test3 = test3;
    }

    /**
     * 测试字段4
     */
    public JSONArray getTest4() {
        return test4;
    }

    public void setTest4(JSONArray test4) {
        this.test4 = test4;
    }

    /**
     *
     */
    public boolean isTest5() {
        return test5;
    }

    public void setTest5(boolean test5) {
        this.test5 = test5;
    }


}
