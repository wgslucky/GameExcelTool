# GameExcelTool

## 项目介绍

在游戏开发中，游戏的数值策划一般使用Excel工具配置游戏中使用到的数据，这样可以方便的对游戏数据进行调整和测试。但是在程序中，为了性能的优化，程序不能直接读取Excel文件获取数据。而是使用工具将Excel数据结构转化为程序中用到的对象，Excel数据转化为Json数据，这样，在游戏客户端或服务器启动的时候，就可以直接读取JSON文件，将数据加载到内存之中。这样也方便游戏数据的动态更新。

本项目使用IDEA工具开发，注意，在将项目导入到IDEA之后，如果发现代码的中文注释变成了乱码，需要把IDEA的Project编码设置为UTF-8

GameExcelTool 工具项目就是这样一个工具，它可以将数值策划配置的数据转化程序需要的对象和数据JSON文件。

## 配置说明

### Excel配置示例

![输入图片说明](https://images.gitee.com/uploads/images/2021/0509/000444_283a684a_23677.png "屏幕截图.png")

### 对字段进地注释

1. 第一行表示生成的类的字段名，字段名后面可以加注释，注释写在括号里面
2. 第二行表示生成类的字段的类型，比如int,int[],string,string[]等
4. 第三行表示字段数据使用的范围，因为有的字段只有客户端会使用，有的字段只有服务器会使用，指定之后可以减少内存占用

### 支持配置的数据类型，即Excel表头字段

1. 基本数据类型: int,string,bool,float,double
2. 基本类型数组：int[],string[],bool[],float[],double[]
3. k-v数据结构：json_obj,例如：{"key":"value"}
4. 对象数组结构：json_array,例如：[{"key":"value"},{"key","value"}]

### 指定字段是否启用
 
在配置Excel表的时候，有些数据只有客户端会使用到，有些数据只有服务器会使用到，另外一些数据是服务器和客户端都会使用到，所以需要让Excel的配置者可以灵活的控制哪些数据只有服务器使用，哪些数据只有客户端使用，哪些数据是服务器和客户端都使用到的数据。这样在游戏服务器或客户端启动的时候，可以选择性的只加载自己使用到的数据，这样可以减少数据的加载，减少内存的占用。

所以可以做以下约定:

* c  表示客户端
* s  表示服务器

在Excel表头中配置c或s，c 表示只有客户端使用，s 表示只有服务器使用，cs表示客户端与服务端都使用。

### 将Excel中配置的数据转化为Json文件

![输入图片说明](https://images.gitee.com/uploads/images/2021/0509/000659_e4d4a88b_23677.png "屏幕截图.png")

### 根据Excel配置生成对应的Java数据类
```java
package com.xinyue.dataconfig;
import com.xinyue.exceltool.logic.codeclass.javacode.IDataConfig;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
/**
 * @Author 王广帅  QQ群：66728073
 */
public class ItemConfig implements IDataConfig {
   private String id;
   private String name;
   private int days;
   private int[] test1;
   private String[] test2;
   private JSONObject test3;
   private JSONArray test4;

   /**
   * 
   */
   public String getId(){
        return id;
   }
   /**
   * 名字
   */
   public String getName(){
        return name;
   }
   /**
   * 
   */
   public int getDays(){
        return days;
   }
   /**
   * 测试字段1
   */
   public int[] getTest1(){
        return test1;
   }
   /**
   * 测试字段2
   */
   public String[] getTest2(){
        return test2;
   }
   /**
   * 测试字段3
   */
   public JSONObject getTest3(){
        return test3;
   }
   /**
   * 测试字段4
   */
   public JSONArray getTest4(){
        return test4;
   }
}
```

## 使用说明

### 修改配置

根据自己的业务和开发环境需要发，可以在applacation.yml中修改配置实现自己的需求，配置对应的类为：

```java

package com.xinyue.exceltool.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "xinyue.config")
public class ExcelToolConfig {
    /**
     * excel 文件所在的目录路径，默认是：config/excel
     */
    private String excelFilePath = "config/excel";
    /**
     * 生成的服务端的配置类目录路径，默认是：config/server_class
     */
    private String serverClassPath = "config/server_class";
    /**
     * 生成的客户端的配置类目录路径，默认是：config/client_class
     */
    private String clientClassPath = "config/client_class";
    /**
     * 生成的服务端的配置数据目录路径，默认是：config/server_data
     */
    private String serverDataPath = "config/server_data";
    /**
     * 生成的客户端配置数据目录路径，默认是：config/client_data;
     */
    private String clientDataPath = "config/client_data";
    /**
     * 是否生成服务端代码和数据
     */
    private boolean serverEnable = true;
    /**
     * 生成的Java代码的包路径
     */
    private String javaClassPackage = "com.xinyue.dataconfig";
    /**
     * 是否生成客户端代码和数据
     */
    private boolean clientEnable = true;
    /**
     * 读取Excel数据时，达到多少条时会刷新到json文件之中
     */
    private int dataFlushThreshold = 3000;
   
    //省略了getter setter方法
}

```
### 打包&运行工具

1. 打包
在项目pom.xml所在目录执行：

```
mvn clean package
```
在target/目录下面获得可以运行的jar包：GameExcelTool-0.0.1-SNAPSHOT.jar

2. 运行

把可运行的jar包(GameExcelTool-0.0.1-SNAPSHOT.jar)和config目录放在同一目录下运行：

```
java -jar GameExcelTool-0.0.1-SNAPSHOT.jar

```
